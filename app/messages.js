const express = require('express');
const messagesDb = require('../messagesDb')
const router = express.Router();


router.get('/', (req, res) => {
    const messages = messagesDb.getMessages();
    res.send(messages);
    
});


router.post('/', (req, res) => {
    messagesDb.addMessage(req.body);
    res.send(req.body)
});


module.exports = router;