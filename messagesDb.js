const fs = require('fs');

const path = "./messages"

let filelist = [];

module.exports = {
    init() {
        filelist = [];
        fs.readdir(path, (err, files) => {
            files.forEach(file => {
                const filedata = fs.readFileSync(path + '/' + file)
                filelist.push(JSON.parse(filedata))
            });
            console.log(filelist)
        });
    },
    getMessages() {      
        return filelist.slice(Math.max(filelist.length - 5, 0))
    },
    addMessage(message) {
        const datetime = new Date().toISOString();
        const newMessage = path + `/${datetime}.txt`;
        message.datetime = datetime;
        fs.writeFileSync(newMessage, JSON.stringify(message, null, 2))
        this.init();
    }
}